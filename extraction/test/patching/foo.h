#ifndef __FOO_H__
#define __FOO_H__

extern void func0(void);
extern void func1(void);
extern void func2(void);
extern void func10(void);

void bar_func0(void);
void bar_func1(void);

#endif
